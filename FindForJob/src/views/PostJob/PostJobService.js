const post2Service = {
  post2List: [
    {
      id: 1,
      name: 'Kavarna Co., Ltd.',
      password: '123',
      address: 'bangsaen',
      email: 'kavarna@gmail.com',
      contact: '0988888888'
    },
    {
      id: 2,
      name: 'Facebook Co., Ltd.',
      password: '123',
      address: 'england',
      email: 'facebook@gmail.com',
      contact: '0977777777'
    }
  ],
  lastId: 3,
  addpost2 (post2) {
    post2.id = this.lastId++
    this.post2List.push(post2)
  },
  updatepost2 (post2) {
    const index = this.post2List.findIndex((item) => item.id === post2.id)
    this.post2List.splice(index, 1, post2)
  },
  deletepost2 (post2) {
    const index = this.post2List.findIndex((item) => item.id === post2.id)
    this.post2List.splice(index, 1)
  },
  getpost2 () {
    return [...this.post2List]
  }
}

export default post2Service
