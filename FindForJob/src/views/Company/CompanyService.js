const companyService = {
  companyList: [
    {
      id: 1,
      name: 'Kavarna Co., Ltd.',
      password: '123',
      address: 'bangsaen',
      email: 'kavarna@gmail.com',
      contact: '0988888888'
    },
    {
      id: 2,
      name: 'Facebook Co., Ltd.',
      password: '123',
      address: 'england',
      email: 'facebook@gmail.com',
      contact: '0977777777'
    }
  ],
  lastId: 3,
  addCompany (company) {
    company.id = this.lastId++
    this.companyList.push(company)
  },
  updateCompany (company) {
    const index = this.companyList.findIndex((item) => item.id === company.id)
    this.companyList.splice(index, 1, company)
  },
  deleteCompany (company) {
    const index = this.companyList.findIndex((item) => item.id === company.id)
    this.companyList.splice(index, 1)
  },
  getCompany () {
    return [...this.companyList]
  }
}

export default companyService
