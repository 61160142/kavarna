import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'users',
    component: () => import('../views/home.vue')
  },
  {
    path: '/company',
    name: 'Company',
    component: () => import('../views/Company/index.vue')
  },
  {
    path: '/postJob',
    name: 'postJob',
    component: () => import('../views/PostJob')
  },
  {
    path: '/formInput',
    name: 'formInput',
    component: () => import('../views/PostJob/formInput.vue')
  },
  {
    path: '/resume',
    name: 'resume',
    component: () => import('../views/Resume/index.vue')
  },
  {
    path: '/jobdetails',
    name: 'jobdetails',
    component: () => import('../views/JobDetails/index.vue')
  },
  {
    path: '/jobdetails/:key',
    name: 'jobdetails',
    component: () => import('../views/JobDetails/index.vue')
  },
  {
    path: '/findjob',
    name: 'findjob',
    component: () => import('../views/FindJob/index.vue')
  },
  {
    path: '/applicant',
    name: 'applicant',
    component: () => import('../views/Applicant/index.vue')
  },
  {
    path: '/companyapply',
    name: 'companyapply',
    component: () => import('../views/CompanyApply/index.vue')
  },
  {
    path: '/selResume',
    name: 'selResume',
    component: () => import('../views/selResume/index.vue')
  },
  {
    path: '/Login',
    name: 'Login',
    component: () => import('../views/Login/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
