const express = require('express')
const resume = require('../Controller/ResumeCon')
const router = express.Router()
router.get('/', resume.getresume)

router.get('/:id', resume.getresumeID)

router.post('/', resume.addResume)

router.put('/', resume.updateresume)

router.delete('/:id', resume.deleteresume)

module.exports = router
