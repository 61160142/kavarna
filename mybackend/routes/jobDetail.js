const express = require('express')
const router = express.Router()
const detail = require('../controller/jobDetailController')
router.get('/',detail.getJob)
router.get('/:key',detail.getJobPosition)
module.exports = router