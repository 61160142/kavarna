const express = require('express')
const companyApply = require('../Controller/CompanyApply')
const router = express.Router()
router.get('/', companyApply.getCompanyApply)

router.get('/:id', companyApply.getCompanyApplyID)

router.post('/', companyApply.addCompanyApply)

router.put('/', companyApply.updateCompanyApply)

router.delete('/:id', companyApply.deleteCompanyApply)

module.exports = router
