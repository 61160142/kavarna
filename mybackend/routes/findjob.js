const express = require('express')
const router = express.Router()
const findJobController = require('../controller/findJobController')
router.get('/',findJobController.getJob)
router.get('/:key',findJobController.getJobPosition)
module.exports = router