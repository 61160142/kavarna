const express = require('express')
const applicantController = require('../Controller/ApplicantController')
const router = express.Router()
router.get('/', applicantController.getApplicant)

router.get('/:id', applicantController.getApplicantID)

router.post('/', applicantController.addApplicant)

router.put('/', applicantController.updateApplicant)

router.delete('/:id', applicantController.deleteApplicant)

module.exports = router
