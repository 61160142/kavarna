const express = require('express')
const postJobService = require('../Controller/PostJobService')
const router = express.Router()
router.get('/', postJobService.getPostJob)

router.get('/:id', postJobService.getPostJobID)

router.post('/', postJobService.addPostJob)

router.put('/', postJobService.updatePostJob)

router.delete('/:id', postJobService.deletePostJob)

module.exports = router
