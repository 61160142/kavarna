const express = require('express')
const companyService = require('../Controller/CompanyService')
const router = express.Router()
router.get('/', companyService.getCompany)

router.get('/:id', companyService.getCompanyID)

router.post('/', companyService.addCompany)

router.put('/', companyService.updateCompany)

router.delete('/:id', companyService.deleteCompany)

module.exports = router
