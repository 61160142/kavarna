const express = require('express')
const MainjobPosting = require('../Controller/MainJobPosting')
const router = express.Router()
router.get('/', MainjobPosting.getJobposting)

router.get('/:id', MainjobPosting.getJobpostingID)

router.post('/', MainjobPosting.addJobposting)

router.put('/', MainjobPosting.updateJobposting)

router.delete('/:id', MainjobPosting.deleteJobposting)

module.exports = router
