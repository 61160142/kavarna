const express = require('express')
const jobPosting = require('../Controller/JobPosting')
const router = express.Router()
router.get('/', jobPosting.getJobposting)

router.get('/:id', jobPosting.getJobpostingID)

router.post('/', jobPosting.addJobposting)

router.put('/', jobPosting.updateJobposting)

router.delete('/:id', jobPosting.deleteJobposting)

module.exports = router
