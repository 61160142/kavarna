const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cors = require('cors')
const mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/kavarna_database', {useNewUrlParser: true,useUnifiedTopology: true})


const companyRouter = require('./routes/companies')
const postjobRouter = require('./routes/postjob')
const jobpostingRouter = require('./routes/jobposting')
const mainjobpostingRouter = require('./routes/mainjobposting')
const resumeRouter = require('./routes/resume')
const applicantRouter = require('./routes/applicant')
const companyApplyRouter = require('./routes/companyapply')
const findJobRouter = require('./routes/findjob')
const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())


app.use('/company', companyRouter)
app.use('/postjob', postjobRouter)
app.use('/formInput', postjobRouter)
app.use('/jobposting', jobpostingRouter)
app.use('/mainjobposting', mainjobpostingRouter)
app.use('/resume', resumeRouter)
app.use('/applicant',applicantRouter)
app.use('/companyapply',companyApplyRouter)
app.use('/findjob',findJobRouter)

module.exports = app
