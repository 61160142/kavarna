const CompanyApply = require('../models/CompanyApply')
const companyapply = {

  async addCompanyApply(req, res) {
    const payload = req.body
    const companyapply = new CompanyApply(payload)
    try {
      await companyapply.save()
      res.status(201).json({
          message:'Add Successfully',
          companyapply
      })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateCompanyApply(req, res) {
    const payload = req.body
    try {
      const companyapply = await CompanyApply.updateOne({ _id: payload._id }, payload)
      res.status(202).json({
        message:'Update Successfully',
        companyapply
    })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteCompanyApply(req, res) {
    const { id } = req.params
    try {
      const companyapply = await CompanyApply.deleteOne({ _id: id })
      res.status(301).json({
        message:'Delete Successfully',
        companyapply
    })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCompanyApply(req, res) {
    try {
      const companyapply = await CompanyApply.find({})
      res.status(200).json(companyapply)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCompanyApplyID(req, res) {
    const { id } = req.params

    try {
      const companyapply = await CompanyApply.findById(id)
      res.status(200).json(companyapply)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = companyapply
