const findJob = require('../models/FindJob')
var position
var type
var recruitment
var working_time
var salary
var gender
var age
var education
var work_experience
var startDate
var endDate
var other_features
const findJobController = {
  async getJob (eq, res) {
    try {
      const search = await findJob.find({})
      res.json(search)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobPosition (req, res) {
    position = req.params.key
    type = position
    work_place = position
    Education_needs = position
    salary = position
    work_time = position
    try {
      findJob
        .find({
          $or: [
            { position: new RegExp(position) },
            { type: new RegExp(type) },
            { work_place: new RegExp(work_place) },
            { Education_needs: new RegExp(Education_needs) },
            { salary: new RegExp(salary) },
            { work_time: new RegExp(work_time) }
          ]
        })
        .then(result => {
          res.json(result)
        })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobType (req, res) {
    position = req.params.key
    type = req.params.key
    work_place = req.params.key
    Education_needs = req.params.key
    salary = req.params.key
    work_time = type
    try {
      findJob
        .find({
          $or: [
            { position: new RegExp(position) },
            { type: new RegExp(position) },
            { work_place: new RegExp(position) },
            { Education_needs: new RegExp(position) },
            { salary: new RegExp(position) },
            { work_time: new RegExp(position) }
          ]
        })
        .then(result => {
          res.json(result)
        })
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = findJobController
