const Company = require('../models/Company')
const companyService = {

  async addCompany(req, res) {
    const payload = req.body
    const company = new Company(payload)
    try {
      await company.save()
      res.status(201).json({
          message:'Add Successfully',
          company
      })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateCompany(req, res) {
    const payload = req.body
    try {
      const company = await Company.updateOne({ _id: payload._id }, payload)
      res.status(202).json({
        message:'Update Successfully',
        company
    })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteCompany(req, res) {
    const { id } = req.params
    try {
      const company = await Company.deleteOne({ _id: id })
      res.status(301).json({
        message:'Delete Successfully',
        company
    })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCompany(req, res) {
    try {
      const company = await Company.find({})
      res.status(200).json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCompanyID(req, res) {
    const { id } = req.params

    try {
      const company = await Company.findById(id)
      res.status(200).json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = companyService
