const ResumeCon = require('../models/Resume')
const Resume = {

  async addResume(req, res) {
    const payload = req.body
    const resume = new ResumeCon(payload)
    try {
      await resume.save()
      res.status(201).json({
          message:'Add Successfully',
          resume
      })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateresume(req, res) {
    const payload = req.body
    try {
      const resume = await ResumeCon.updateOne({ _id: payload._id }, payload)
      res.status(202).json({
        message:'Update Successfully',
        resume
    })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteresume(req, res) {
    const { id } = req.params
    try {
      const resume = await ResumeCon.deleteOne({ _id: id })
      res.status(301).json({
        message:'Delete Successfully',
        resume
    })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getresume(req, res) {
    try {
      const resume = await ResumeCon.find({})
      res.status(200).json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getresumeID(req, res) {
    const { id } = req.params

    try {
      const resume = await ResumeCon.findById(id)
      res.status(200).json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = Resume
