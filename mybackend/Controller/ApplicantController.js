const Applicant = require('../models/Applicant')
const applicant = {

  async addApplicant(req, res) {
    const payload = req.body
    const applicant = new Applicant(payload)
    try {
      await applicant.save()
      res.status(201).json({
          message:'Add Successfully',
          applicant
      })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateApplicant(req, res) {
    const payload = req.body
    try {
      const applicant = await Applicant.updateOne({ _id: payload._id }, payload)
      res.status(202).json({
        message:'Update Successfully',
        applicant
    })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteApplicant(req, res) {
    const { id } = req.params
    try {
      const applicant = await Applicant.deleteOne({ _id: id })
      res.status(301).json({
        message:'Delete Successfully',
        applicant
    })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getApplicant(req, res) {
    try {
      const applicant = await Applicant.find({})
      res.status(200).json(applicant)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getApplicantID(req, res) {
    const { id } = req.params

    try {
      const applicant = await Applicant.findById(id)
      res.status(200).json(applicant)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = applicant
