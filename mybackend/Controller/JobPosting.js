const Jobposting = require('../models/Jobposting')
const JobPosting = {

  async addJobposting(req, res) {
    const payload = req.body
    const jobposting = new Jobposting(payload)
    try {
      await jobposting.save()
      res.status(201).json({
          message:'Add Successfully',
          jobposting
      })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateJobposting(req, res) {
    const payload = req.body
    try {
      const jobposting = await Jobposting.updateOne({ _id: payload._id }, payload)
      res.status(202).json({
        message:'Update Successfully',
        jobposting
    })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteJobposting(req, res) {
    const { id } = req.params
    try {
      const jobposting = await Jobposting.deleteOne({ _id: id })
      res.status(301).json({
        message:'Delete Successfully',
        jobposting
    })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobposting(req, res) {
    try {
      const jobposting = await Jobposting.find({})
      res.status(200).json(jobposting)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobpostingID(req, res) {
    const { id } = req.params

    try {
      const jobposting = await Jobposting.findById(id)
      res.status(200).json(jobposting)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = JobPosting
