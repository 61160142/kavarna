const PostJob = require('../models/PostJob')
const postJobService = {

  async addPostJob(req, res) {
    const payload = req.body
    const postJob = new PostJob(payload)
    try {
      await postJob.save()
      res.status(201).json({
        message: 'Add Successfully',
        postJob
      })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updatePostJob(req, res) {
    const payload = req.body
    try {
      const postJob = await PostJob.updateOne({ _id: payload._id }, payload)
      res.status(202).json({
        message: 'Update Successfully',
        postJob
      })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deletePostJob(req, res) {
    const { id } = req.params
    try {
      const postJob = await PostJob.deleteOne({ _id: id })
      res.status(301).json({
        message: 'Delete Successfully',
        postJob
      })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getPostJob(req, res) {
    try {
      const postJob = await PostJob.find({})
      res.status(200).json(postJob)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getPostJobID(req, res) {
    const { id } = req.params

    try {
      const postJob = await PostJob.findById(id)
      res.status(200).json(postJob)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = postJobService
