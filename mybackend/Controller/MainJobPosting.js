const Mainjobposting = require('../models/Mainjobposting')
const MainJobPosting = {

  async addJobposting(req, res) {
    const payload = req.body
    const mainjobposting = new Mainjobposting(payload)
    try {
      await mainjobposting.save()
      res.status(201).json({
          message:'Add Successfully',
          mainjobposting
      })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateJobposting(req, res) {
    const payload = req.body
    try {
      const mainjobposting = await Mainjobposting.updateOne({ _id: payload._id }, payload)
      res.status(202).json({
        message:'Update Successfully',
        mainjobposting
    })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteJobposting(req, res) {
    const { id } = req.params
    try {
      const mainjobposting = await Mainjobposting.deleteOne({ _id: id })
      res.status(301).json({
        message:'Delete Successfully',
        mainjobposting
    })
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobposting(req, res) {
    try {
      const mainjobposting = await Mainjobposting.find({})
      res.status(200).json(mainjobposting)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobpostingID(req, res) {
    const { id } = req.params

    try {
      const mainjobposting = await Mainjobposting.findById(id)
      res.status(200).json(mainjobposting)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = MainJobPosting
