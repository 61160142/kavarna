const mongoose = require('mongoose')
const applicantSchema = new mongoose.Schema({
      id: String,
      username: String,
      email: String,
      password: String,
      confirm_email: String,
      confirm_password: String
})

module.exports = mongoose.model('applicant', applicantSchema)
