const mongoose = require('mongoose')
const MainjobPostingSchema = new mongoose.Schema({
      id: String,
      job_position: String,
      job_type: String,
      number_recept: String,
      date_start:Date,
      date_end:Date
})

module.exports = mongoose.model('mainjobposting', MainjobPostingSchema)
