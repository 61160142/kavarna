const mongoose = require('mongoose')
const resumeSchema = new mongoose.Schema({
      id: String,
      nametitle: String,
      name: String,
      surname: String,
      birthdate: Date,
      card_ID: String,
      age:String,
      height:String,
      weight:String,
      gender:String,
      maritalstatus:String,
      nationality:String,
      religion:String,
      phonenumber:String,
      address:String,
      country:String,
      duration:String,
      graduation:String,
      countryofstudy:String,
      educationlevel:String,
      educationlinstitution:String,
      educational:String,
      fieldofstudy:String,
      gpa:String,
      detailwork:String,
      skill:String
})

module.exports = mongoose.model('resume', resumeSchema)
