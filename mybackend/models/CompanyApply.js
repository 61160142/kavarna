const mongoose = require('mongoose')
const companyApplySchema = new mongoose.Schema({
      id: String,
      username: String,
      password: String,
      confirm_password: String,
      name_company: String,
      type_business:String,
      name_contact:String,
      company_address:String,
      tel:String,
      email:String
})

module.exports = mongoose.model('companyapply', companyApplySchema)
