const mongoose = require('mongoose')
const findSchema = new mongoose.Schema({
  position: String,
  type: String,
  province: String,
  education: String,
  moneyStart: String,
  moneyEnd: String,
  timeIn: String,
  timeOut: String
})

module.exports = mongoose.model('postjob', findSchema)
