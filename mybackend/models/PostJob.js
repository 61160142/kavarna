const mongoose = require('mongoose')
const postJobSchema = new mongoose.Schema({
      id: String,
      classification: String,
      category: String,
      position: String,
      rate: String,
      restDay: {},
      timeIn: String,
      timeOut: String,
      otherTime: String,
      moneyStart: String,
      moneyEnd: String,
      province: String,
      district: String,
      workDetail: String,
      welfare: String,
      otherWelfare: String,
      gender: String,
      ageMin: String,
      ageMax: String,
      education: String,
      experience: String,
      otherProperty: String,
      postOpen: String,
      postClose: String,
})

module.exports = mongoose.model('postJob', postJobSchema)
