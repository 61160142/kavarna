const mongoose = require('mongoose')
const jobPostingSchema = new mongoose.Schema({
      id: String,
      job_category: String,
      job_position: String,
      job_type: String,
      number_recept: String,
      holiday: String,
      working_begin:String,
      working_end:String,
      other_working:String,
      salary_min:String,
      salary_max:String,
      province:String,
      district:String,
      expedition:String,
      responsibility:String,
      benefit:String,
      other_benefit:String,
      gender:String,
      age_min:String,
      age_max:String,
      education:String,
      experience:String,
      qualification:String,
      date_start:Date,
      date_end:Date,
      name_contact:String,
      tel_contact:String,
      email_contact:String
})

module.exports = mongoose.model('jobpostings', jobPostingSchema)
