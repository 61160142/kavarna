const mongoose = require('mongoose')
const companySchema = new mongoose.Schema({
      id: String,
      name: String,
      password: String,
      address: String,
      email: String,
      contact: String
})

module.exports = mongoose.model('companies', companySchema)
